package decorator.exercice;

public class AbstractAccountDecorator extends AbstractAccount {

    protected AbstractAccount decoratedAccount;


    public AbstractAccountDecorator(AbstractAccount decoratedAccount) {
        this.decoratedAccount = decoratedAccount;
    }


    @Override
    public void verifyAccount() {
        decoratedAccount.verifyAccount();
    }

}
