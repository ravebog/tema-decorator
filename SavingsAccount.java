package decorator.exercice;

public class SavingsAccount extends AbstractAccount {

    public void setBalanta(double balanta) {
        if (this.balanta < 0) {
            throw new IllegalArgumentException();
        } else {
            this.balanta = balanta;
        }
    }

    @Override
    public void verifyAccount() {
        System.out.println("Aveti " + this.balanta + " lei in cont");
    }


}
