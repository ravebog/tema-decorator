package decorator.exercice;

public class SmsManageableAccountDecorator extends AbstractAccountDecorator {

    public SmsManageableAccountDecorator(AbstractAccount decoratedAccount) {
        super(decoratedAccount);
    }

    @Override
    public void verifyAccount() {
        sendSmsToCustomer(decoratedAccount);
        decoratedAccount.setBalanta(this.balanta);
        decoratedAccount.verifyAccount();


    }

    private void sendSmsToCustomer(AbstractAccount decoratedAccount) {

            System.out.println("Contul dumneavoastra beneficiaza de optiunea trimitere SMS");


    }

}
