package decorator.exercice;

public abstract class AbstractAccount {
    double balanta;
    public abstract void verifyAccount();

    public double getBalanta() {
        return balanta;
    }

    public void setBalanta(double balanta) {
        this.balanta = balanta;
    }

    @Override
    public String toString() {
        return "AbstractAccount{" +
                "balanta=" + balanta +
                '}';
    }
}
