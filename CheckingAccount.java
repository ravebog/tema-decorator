package decorator.exercice;

public class CheckingAccount extends AbstractAccount {
    private double overdraft;

    public CheckingAccount(double overdraft) {
        this.overdraft = overdraft;
    }

    public void setBalanta(double balanta) {
        if (this.balanta + this.overdraft < -50) {
            throw new IllegalArgumentException();
        } else {
            this.balanta = balanta;
        }
    }

    @Override
    public void verifyAccount() {
        System.out.println("Aveti " + this.balanta + " lei in cont");

    }
}

