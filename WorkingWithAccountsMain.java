package decorator.exercice;

public class WorkingWithAccountsMain {
    public static void main(String[] args) {

        AbstractAccount savingsAccount = new SavingsAccount();
        AbstractAccount checkingAccount = new CheckingAccount(50.0);
        AbstractAccount smsManageablSavingsAccount = new SmsManageableAccountDecorator(new SavingsAccount());
        AbstractAccount smsManageableCheckingAccount = new SmsManageableAccountDecorator(new CheckingAccount(50.0));

        savingsAccount.setBalanta(50);

        System.out.println("Verif pt savings account ");
        savingsAccount.verifyAccount();
        System.out.println("############");


        checkingAccount.setBalanta(-50);
        System.out.println("Verif pt checking account ");
        checkingAccount.verifyAccount();
        System.out.println("############");

        smsManageablSavingsAccount.setBalanta(100);
        System.out.println("Verificare pt savings account decorat ");
        smsManageablSavingsAccount.verifyAccount();
        System.out.println("############");

        smsManageableCheckingAccount.setBalanta(100);
        System.out.println("Verificare pt checking account decorat ");
        smsManageableCheckingAccount.verifyAccount();


    }


}
